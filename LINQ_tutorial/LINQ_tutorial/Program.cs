﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LINQ_tutorial
{
    class Program
    {
        static void Main(string[] args)
        {
             // Create a data source by using a collection initializer.

               List<Student> students = new List<Student>
               {
                new Student {First="Svetlana", Last="Omelchenko", ID=111, Scores= new List<int> {97, 92, 81, 60}},
                new Student {First="Claire", Last="O'Donnell", ID=112, Scores= new List<int> {75, 84, 91, 39}},
                new Student {First="Sven", Last="Mortensen", ID=113, Scores= new List<int> {88, 94, 65, 91}},
                new Student {First="Cesar", Last="Garcia", ID=114, Scores= new List<int> {97, 89, 85, 82}},
                new Student {First="Debra", Last="Garcia", ID=115, Scores= new List<int> {35, 72, 91, 70}},
                new Student {First="Fadi", Last="Fakhouri", ID=116, Scores= new List<int> {99, 86, 90, 94}},
                new Student {First="Hanying", Last="Feng", ID=117, Scores= new List<int> {93, 92, 80, 87}},
                new Student {First="Hugo", Last="Garcia", ID=118, Scores= new List<int> {92, 90, 83, 78}},
                new Student {First="Lance", Last="Tucker", ID=119, Scores= new List<int> {68, 79, 88, 92}},
                new Student {First="Terry", Last="Adams", ID=120, Scores= new List<int> {99, 82, 81, 79}},
                new Student {First="Eugene", Last="Zabokritski", ID=121, Scores= new List<int> {96, 85, 91, 60}},
                new Student {First="Michael", Last="Tucker", ID=122, Scores= new List<int> {94, 92, 91, 91} }
                };
            
            IEnumerable<Student> studentQuery = 
                from student in students
                where student.Scores[0] > 90 && student.Scores[3] > 80
                orderby student.Last ascending                              // по-возврастанию
                select student;

            foreach (var student in studentQuery)
            {
                Console.WriteLine("{0} {1} {2}", student.Last, student.First,student.Scores[0]);
            }

            //second query. grouping students by first letters of last names
            var studentQuery2 = from student in students 
                                group student by student.Last[0];

            foreach (var studentGroup in studentQuery2)
            {
                Console.WriteLine(studentGroup.Key);
                foreach (var student in studentGroup)
                {
                    Console.WriteLine("     {0} {1}", student.Last,student.First);
                }
            }

            //third query
            var studentQuery4 =
                from student in students
                group student by student.Last[0] into studentGroup
                orderby studentGroup.Key
                select studentGroup;
            
            foreach (var groupOfStudents in studentQuery4)
            {

                Console.WriteLine(groupOfStudents.Key);

                foreach (var student in groupOfStudents)
                {
                    Console.WriteLine("   {0}, {1}", student.Last, student.First);
                }
            }

            //fourth query
            Console.WriteLine("\n========================");
            var studQuery5 =
                from student in students
                let totalScore = student.Scores[0] + student.Scores[1] + student.Scores[2] + student.Scores[3]
                where totalScore/4 < student.Scores[0]
                orderby student.Scores[0]
                select student.Last + " " + student.First +" "+ student.Scores[0];
            foreach (var stud in studQuery5)
            {
                Console.WriteLine(stud);
            }

            //fifth query
            Console.WriteLine("\n========================");

            var studQuery6 = 
                from stud in students
                let totalScore = stud.Scores[0] + stud.Scores[1] + stud.Scores[2] + stud.Scores[3]
                select totalScore;

            double averageScore = studQuery6.Average();
            Console.WriteLine(averageScore);

            //sixth query
            Console.WriteLine("\n=======================");

            var studQuery7 =
                from stud in students
                let x = stud.Scores[0] + stud.Scores[1] + stud.Scores[2] + stud.Scores[3]
                where x > averageScore
                select new {id = stud.ID, score = x};
            foreach (var ss in studQuery7)
            {
                Console.WriteLine(ss.id, ss.score);
            }
        }
    }
}
